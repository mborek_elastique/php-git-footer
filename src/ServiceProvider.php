<?php

namespace Wocozon\GitFooter;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/git-footer.php' => config_path('git-footer.php'),
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/config/git-footer.php', 'git-footer');

        $this->app->bind('wocozon:git-footer', Console\Commands\UpdateGitFooter::class);

        $this->commands([
            'wocozon:git-footer',
        ]);
    }
}
