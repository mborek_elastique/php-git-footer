<?php

namespace Wocozon\GitFooter\Helpers;

class GitFooter
{
    public static function getFormattedString(): string
    {
        $file    = config('git-footer.file_path');
        $modtime = filemtime(base_path() . '/vendor/autoload.php');
        $updated = \Carbon\Carbon::now()->locale('en')->subSeconds(\Carbon\Carbon::now()->unix() - $modtime)->diffForHumans();
        $suffix  = " [deployed: $updated]";

        if (file_exists($file)) {
            $out = file_get_contents($file) . $suffix;
        } else {
            $out = "No git information available $suffix";
        }

        return $out;
    }
}
